<?php

/**
 * OK I'm not entierly sure if I need singelton here but having instance and strong reference to $dm has been proven to work!
 */

class dm_ExtendedFunctionality {

    private static $instance = null;

    public static function get_instance() {
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct() {}

    private $dm = null;

    public function init($dm) {
        $this->dm = $dm;

    }

}