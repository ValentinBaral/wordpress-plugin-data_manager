<?php
/**
 * data_manager
 *
 *
 *
 * @link              http://www.surfaceimpression.digital
 * @since             0.1
 * @package           data_manager
 *
 * @wordpress-plugin
 * Plugin Name:       data_manager
 * Plugin URI:        http://www.surfaceimpression.digital
 * Description:       Manage data structure
 * Version:           0.2
 * Author:            Valentin Baral
 * Author URI:        http://www.surfaceimpression.digital
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       data_manager
 * Domain Path:       /languages
 */

if (!defined('ABSPATH')) {
	exit;
}

if (!is_admin()) {
	include_once ABSPATH . 'wp-admin/includes/plugin.php';
}

// there was a problem once where the time zone would be wrong when converting timestamp to date in php
date_default_timezone_set("Europe/London");

/**
 * Main plugin class
 *
 * Initiates all modular parts of the plugin and stores core data structure
 *
 * TODO: Think about putting all post/type logic into core_data
 * TODO: Add error handling and more logging
 * TODO: Generally module classes wouldn't have to be singoltons as I could always refer to their instances through this class for filters
 */

class dm_DataManager {

	private static $instance = null;

	public static function get_instance() {
		if (null == self::$instance) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * prefix prepended onto all post types, taxonomies, fields, filters
	 * @var string
	 */
	public $prefix = 'dm';

	/**
	 * references to all post types
	 * @var array [dm_PostType]
	 */
	public $postTypes = [];

	/**
	 * references to all fields
	 * @var array [dm_Field]
	 */
	public $fields = [];

	/**
	 * references to all taxonomies
	 * @var array [dm_Taxonomy]
	 */
	public $taxonomies = [];

	/**
	 * which plugins are activate (this is checked later on)
	 * TODO: These should be automated; include plugin path
	 * @var array
	 */
	public $pluginSupport = [
		'polylang' => false,
		'posts-to-posts' => false,
	];

	/**
	 * Reference to open log file
	 * @var resource
	 */
	private $logFile;

	/**
	 * Filename of log file
	 * @var string
	 */
	private $logFilename = 'dm_general_log.txt';

	/**
	 * Thse are all module components which handle the various parts of this plugin
	 * @var array
	 */
	public $modules = [];

	private function __construct() {
		if (!defined('SI_PLUGIN_PATH')) {
			define('SI_PLUGIN_PATH', plugin_dir_path(__FILE__));
		}

		if (!defined('SI_PLUGIN_URL')) {
			define('SI_PLUGIN_URL', plugin_dir_url(__FILE__));
		}

		require_once SI_PLUGIN_PATH . '/includes/classes/dm_post_type.php';
		require_once SI_PLUGIN_PATH . '/includes/classes/dm_field.php';
		require_once SI_PLUGIN_PATH . '/includes/classes/dm_taxonomy.php';

		// include core moduel classes
		require_once SI_PLUGIN_PATH . '/includes/classes/modules/core/core_data/dm_core_data.php';
		require_once SI_PLUGIN_PATH . '/includes/classes/modules/core/api/dm_api.php';

		// include plugin config (function)
		require_once SI_PLUGIN_PATH . '/config.php';

		// include site install specific functionality extension module
		require_once SI_PLUGIN_PATH . '/extend.php';
	}

	public function init() {

		// set up native structure
		$this->defineStructure();

		// check plugin support
		$this->pluginSupport['polylang'] = is_plugin_active('polylang/polylang.php');
		$this->pluginSupport['posts-to-posts'] = is_plugin_active('posts-to-posts/posts-to-posts.php');

		// setup module components
		$this->modules['core_data'] = dm_CoreData::get_instance();
		$this->modules['api'] = dm_Api::get_instance();

		if (is_admin()) {
			require_once SI_PLUGIN_PATH . '/includes/classes/modules/core/admin/dm_admin.php';
			$this->modules['admin'] = dm_Admin::get_instance();
		}

		if ($this->pluginSupport['polylang']) {
			require_once SI_PLUGIN_PATH . '/includes/classes/modules/extensions/multi_lang/dm_multi_lang.php';
			$this->modules['multilang'] = dm_MultiLang::get_instance();
		}
		if ($this->pluginSupport['posts-to-posts']) {
			require_once SI_PLUGIN_PATH . '/includes/classes/modules/extensions/connections/dm_connections.php';
			$this->modules['connections'] = dm_Connections::get_instance();
		}

		$this->modules['extended_functionality'] = dm_ExtendedFunctionality::get_instance();

		if (function_exists(dm_custom_prefix)) {
			// read config
			$this->prefix = dm_custom_prefix($this->prefix);
		} else {
			//log error
		}

		// initiate modules, usually order should matter here?
		// TODO: Enforece or check for init function, to avoid errors
		foreach ($this->modules as $name => $module) {
			$module->init(self::$instance);
		}

		if (function_exists(dm_user_define_structure)) {
			// read config
			dm_user_define_structure(self::$instance);
		} else {
			//log error
			$this->log('config not defined', __LINE__, __FUNCTION__, __CLASS__, true);
		}

		// make getters availabe per filter
		add_filter($this->prefix . '_get_field', array(self::$instance, 'getFieldForName'), 10, 1);
		add_filter($this->prefix . '_get_post_type', array(self::$instance, 'getPostTypeForName'), 10, 1);
		add_filter($this->prefix . '_get_taxonomy', array(self::$instance, 'getTaxonomyForName'), 10, 1);

		// global logging function ?
		add_filter($this->prefix . '_log', array(self::$instance, 'log'), 10, 4);

	}

	/**
	 * define initial data structure to make use of nativ wordpress type within our system
	 */
	private function defineStructure() {

		// post types to refer to native post types
		$posts = $this->createPostType('post', array('native_type' => true, 'kind' => 'post'), array('label' => 'Posts'));
		$pages = $this->createPostType('page', array('native_type' => true, 'kind' => 'post'), array('label' => 'Pages'));
		$attachments = $this->createPostType('attachment', array('native_type' => true, 'kind' => 'attachment'), array('label' => 'Attachments'));
		$users = $this->createPostType('user', array('native_type' => true, 'kind' => 'user'), array('label' => 'Users'));
		$comment = $this->createPostType('comment', array('native_type' => true, 'kind' => 'comment'), array('label' => 'Comment'));

		// add taxonomies to make those available in functions
		$tags = $this->createTaxonomy('post_tag', array('native_taxonomy' => true, 'kind' => 'term'), array('label' => 'Tags'));
		$categories = $this->createTaxonomy('category', array('native_taxonomy' => true, 'kind' => 'term'), array('label' => 'Categories'));

		$posts->addTaxonomy($categories);
		$posts->addTaxonomy($tags);

	}

	/**
	 * Create post type object and store reference, post types are registered in core_data on filter hook
	 * @param string name of the post type
	 * @param array plugin specific args
	 * @param array to overwrite wordpress arguments (optional)
	 * @return object dm_PostType $postType
	 */
	public function createPostType($name, $args, $overwriteArgs = array()) {

		$postType = new dm_PostType($name, $args, $overwriteArgs);
		$this->postTypes[] = $postType;
		return $postType;

	}

	/**
	 * Create field object and store reference
	 * @param string name of the field
	 * @param array plugin specific args
	 * @return object dm_Field $field
	 */
	public function createField($name, $args) {

		$field = new dm_Field($name, $args);
		$this->fields[] = $field;
		return $field;

	}

	/**
	 * Create taxonomy object and store reference
	 * @param string name of the taxonomy
	 * @param array plugin specific args
	 * @return object dm_Taxonomy $taxonomy
	 */
	public function createTaxonomy($name, $args, $overwriteArgs) {

		$taxonomy = new dm_Taxonomy($name, $args, $overwriteArgs);
		$this->taxonomies[] = $taxonomy;
		return $taxonomy;

	}

	/**
	 * Get taxonomy by name reference
	 * @param string name of the taxonomy
	 * @return object dm_Taxonomy $taxonomy | null
	 */
	public function getTaxonomyForName($taxonomyName) {

		$name = $taxonomyName;

		$prefixString = $this->prefix . '_';
		$prefixPos = strpos($taxonomyName, $prefixString);

		if ($prefixPos > -1) {
			$name = substr($taxonomyName, $prefixPos . strlen($prefixString));
		}

		for ($t = 0; $t < count($this->taxonomies); $t++) {
			$tmpTaxonomy = $this->taxonomies[$t];

			if ($tmpTaxonomy->name === $name) {
				return $tmpTaxonomy;
			}

		}

		return null;

	}

	/**
	 * Get post type by name reference
	 * @param string name of the post type
	 * @return object dm_PostType $postType | null
	 */
	public function getPostTypeForName($postTypeName) {

		$name = $postTypeName;

		$prefixString = $this->prefix . '_';
		$prefixPos = strpos($postTypeName, $prefixString);

		if ($prefixPos > -1) {
			$name = substr($postTypeName, $prefixPos . strlen($prefixString));
		}

		for ($p = 0; $p < count($this->postTypes); $p++) {
			$postType = $this->postTypes[$p];

			if ($postType->name === $name) {
				return $postType;
			}

		}

		return null;

	}

	/**
	 * Get field by name reference
	 * @param string name of the field
	 * @return object dm_Field $field | null
	 */
	public function getFieldForName($fieldName) {

		$name = $fieldName;

		$prefixString = $this->prefix . '_';
		$prefixPos = strpos($fieldName, $prefixString);

		if ($prefixPos > -1) {
			$name = substr($fieldName, $prefixPos . strlen($prefixString));
		}

		for ($f = 0; $f < count($this->fields); $f++) {
			$field = $this->fields[$f];

			if ($field->name === $name) {
				return $field;
			}

		}

		return null;

	}

	/**
	 * Standard logging function
	 * @param object $toLog – Any variables/data/anything, check if string or object
	 * @param int $line (optional) – The line number the function was called from
	 * @param string $function (optional) – Function name
	 * @param string $class (optional) – Class name
	 * @param boolean $printTime (optional) – Add time if true
	 */
	public function log($toLog, $line = -1, $function = '', $class = '', $printTime = false) {

		if (!(isset($this->logFile) || is_resource($this->logFile))) {
			$this->logFile = @fopen(SI_PLUGIN_PATH . '/includes/logs/' . $this->logFilename, 'a');
		}

		if (isset($this->logFile) && is_resource($this->logFile)) {

			$out = '';

			if ($printTime) {
				$out .= 'logged @ ' . date("F j, Y, g:i a");
				$out .= "\n";
			}

			$out .= ($line > -1 ? $line . ' ' : '') . ($function != '' ? '[' . ($class != '' ? $class . '::' : '') . $function . '] ' : '');

			if (is_string($toLog)) {
				$out .= $toLog;
			} else {
				$out .= print_r($toLog, true);
			}

			$out .= "\n";

			fwrite($this->logFile, $out);

		}
	}

}

add_action('plugins_loaded', array(dm_DataManager::get_instance(), 'init'));
