<?php

class dm_PostType {

	public $name;
	public $args;
	public $overwriteArgs;

	//public $connections;
	public $fields;
	public $taxonomies;

	function __construct($name, $args, $overwriteArgs) {

		$this->name = $name;
		$this->args = $args;
		$this->overwriteArgs = $overwriteArgs;

		$this->fields = [];
		$this->taxonomies = [];

	}

	public function addTaxonomy($taxonomy) {
		$this->taxonomies[] = $taxonomy;
	}

	public function addField($field) {
		$this->fields[] = $field;
	}

}
