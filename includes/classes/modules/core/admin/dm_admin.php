<?php

/**
 * Show backend meta boxes
 * Add admin filters to save values in core_data
 */

class dm_Admin {

	private static $instance = null;

	public static function get_instance() {
		if (null == self::$instance) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private $dm = null;

	public function init($dm) {
		$this->dm = $dm;

		add_action('add_meta_boxes', array(self::$instance, 'buildCustomFieldsMetaBoxes'));

		// customize backend
		add_action('admin_head', array(self::$instance, 'addDynamicAdminStyles'));
		add_action('admin_enqueue_scripts', array(self::$instance, 'enqueueScripts'));

		add_action('post_edit_form_tag', array(self::$instance, 'addFormMultipartEncoding'), 10, 1);

		add_action('edit_user_profile', array(self::$instance, 'buildUserForm'));
		add_action('show_user_profile', array(self::$instance, 'buildUserForm'));
		add_action('user_new_form', array(self::$instance, 'buildNewUserForm'));
		add_action('user_new_form_tag', array(self::$instance, 'addFormMultipartEncoding'));
		add_action('user_edit_form_tag', array(self::$instance, 'addFormMultipartEncoding'));

		add_action('add_meta_boxes_comment', array(self::$instance, 'buildCommentForm'));

		// thsese filters are only relevant in backend, though saving fields might still be required at other places
		if (array_key_exists('core_data', $this->dm->modules)) {

			// save post meta
			add_action('save_post', array(self::$instance->dm->modules['core_data'], 'saveCustomFieldsForPostType'));
			add_action('edit_attachment', array(self::$instance->dm->modules['core_data'], 'saveCustomFieldsForPostType'));

			// save user meta
			add_action('personal_options_update', array(self::$instance->dm->modules['core_data'], 'saveCustomFieldsForUser'));
			add_action('edit_user_profile_update', array(self::$instance->dm->modules['core_data'], 'saveCustomFieldsForUser'));
			add_action('user_register', array(self::$instance->dm->modules['core_data'], 'saveCustomFieldsForUser'));
			add_action('custom_save_user_fields', array(self::$instance->dm->modules['core_data'], 'saveCustomFieldsForUser'));

			// save taxonomy meta
			add_action('create_term', array(self::$instance->dm->modules['core_data'], 'saveCustomFieldsForTaxonomy'), 10, 3);
			add_action('edit_term', array(self::$instance->dm->modules['core_data'], 'saveCustomFieldsForTaxonomy'), 10, 3);

			// save comment meta
			add_action('post_comment', array(self::$instance->dm->modules['core_data'], 'saveCustomFieldsForComment'), 10, 3);
			add_action('edit_comment', array(self::$instance->dm->modules['core_data'], 'saveCustomFieldsForComment'), 10, 2);

		}

	}

	/**
	 * build form for new taxonomy term
	 * @param string $taxonomyName
	 */
	public function buildNewTermForm($taxonomyName) {
		$this->buildTermForm(null, $taxonomyName);
	}

	/**
	 * build form for taxonomy term
	 * @param wp_term? $term
	 * @param string $taxonomyName
	 */
	public function buildTermForm($term, $taxonomyName) {

		$taxonomy = $this->dm->getTaxonomyForName($taxonomyName);
		if ($taxonomy == null) {
			return;
		}

		for ($c = 0; $c < count($taxonomy->fields); $c++) {

			$tmpField = $taxonomy->fields[$c];

			$args = array();

			$args['field'] = $tmpField;

			$args['type'] = 'term';

			$termObj = null;

			if ($term != null) {
				// for consistency
				$termObj = new stdClass();
				$termObj->ID = $term->term_id;
			}

			$this->buildMetaBox($termObj, array('args' => $args));

		}

	}

	/**
	 * Build form containing defined fields when creating new user
	 */
	public function buildNewUserForm() {
		$this->buildUserForm(null);
	}

	/**
	 * Build form containing defined fields when editing or creating user
	 * @param wp_user $user | null
	 */
	public function buildUserForm($user) {

		$users = $this->dm->getPostTypeForName('user');
		if ($users == null) {
			return;
		}

		for ($c = 0; $c < count($users->fields); $c++) {

			$tmpField = $users->fields[$c];

			$args = array();

			$args['field'] = $tmpField;

			$args['type'] = 'user';

			$termObj = null;

			if ($user != null) {
				// for consistency
				$termObj = new stdClass();
				$termObj->ID = $user->ID;
			}

			$this->buildMetaBox($termObj, array('args' => $args));

		}

	}

	/**
	 * Build form containing defined fields when editing or creating comment
	 * @param wp_comment $comment
	 */
	public function buildCommentForm($comment) {

		$comments = $this->dm->getPostTypeForName('comment');
		if ($comments == null) {
			return;
		}

		for ($c = 0; $c < count($comments->fields); $c++) {

			$tmpField = $comments->fields[$c];

			$args = array();

			$args['field'] = $tmpField;

			$args['type'] = 'comment';

			$termObj = null;

			if ($comment != null) {
				// for consistency
				$termObj = new stdClass();
				$termObj->ID = $comment->comment_ID;
			}

			$this->buildMetaBox($termObj, array('args' => $args));

		}

	}

	/**
	 * Build meta boxes in wordpress for fields referenced in post type
	 * Calling wordpress's add_meta_box which will later call buildMetaBox in a callback
	 * @param string $postTypeName
	 */
	public function buildCustomFieldsMetaBoxes($postTypeName) {

		$postType = $this->dm->getPostTypeForName($postTypeName);

		$postTypeFields = $postType->fields;

		for ($c = 0; $c < count($postTypeFields); $c++) {

			$tmpField = $postTypeFields[$c];

			$args = array();

			$args['field'] = $tmpField;

			$position = 'side';

			if (array_key_exists('position', $tmpField->args)) {
				$position = $tmpField->args['position'];
			}

			$label = $tmpField->name;

			if (array_key_exists('label', $tmpField->args)) {
				$label = $tmpField->args['label'];
			}

			add_meta_box(
				$this->dm->prefix . '_' . $tmpField->name . '_box',
				$label,
				array(self::$instance, 'buildMetaBox'),
				//self::$prefix.'_'.$postType->name,
				null,
				$position,
				'default',
				$args
			);

		}

	}

	/**
	 * Build and display individual meta box contains some hardcoded functionality from older sites?
	 * @param wp_post $post – This is included becasue this is called from a callback in add_meta_box
	 * @param array $args – Containing args passed in add_meta_box (oddly under another 'args' key)
	 */
	public function buildMetaBox($post, $args) {

		$field = $args['args']['field'];

		// for some reason callback_args array is not what is passed in add_meta_box function
		$type = array_key_exists('type', $args['args']) ? $args['args']['type'] : 'post';

		if (!is_admin()) {

			if (array_key_exists('admin_field', $field->args)) {
				if ($field->args['admin_field'] == true) {
					return;
				}
			}

		}

		// echo description here if set
		if (!is_admin() || $type == 'user' || $type == 'comment') {

			$label = $field->name;

			if (array_key_exists('label', $field->args)) {
				$label = $field->args['label'];
			}

			$fieldId = $this->dm->prefix . '_' . $field->name;

			echo '<label for="' . $fieldId . '">' . $label . '</label>';

			if (array_key_exists('form_description', $field->args)) {
				echo '<p>';
				echo $field->args['form_description'];
				echo '</p>';
			}

		} else if (isset($field->args['description'])) {

			echo '<p>';
			echo $field->args['description'];
			echo '</p>';
		}

		echo '<div class="' . $this->dm->prefix . '_field">';

		switch ($field->args['type']) {

		case 'number':
			$this->metaBox_number($post, $args['args']);
			break;

		case 'text':
			$this->metaBox_text($post, $args['args']);
			break;

		case 'extendedText':
			$this->metaBox_extendedText($post, $args['args']);
			break;

		case 'option':
			$this->metaBox_option($post, $args['args']);
			break;

		case 'options':
			$this->metaBox_options($post, $args['args']);
			break;

		case 'choose':
			$this->metaBox_choose($post, $args['args']);
			break;

		case 'imageSelection':
			$this->metaBox_imageSelection($post, $args['args']);
			break;

		case 'file':
			$this->metaBox_file($post, $args['args']);
			break;

		case 'date':
			$this->metaBox_date($post, $args['args']);
			break;

		default:
			$this->metaBox_text($post, $args['args']);
			break;

		}

		echo '</div>';

	}

	/**
	 * TODO: need to simplify these
	 * 	-> option would be to combne in one methode
	 * 	-> problem is that some are quite different in functionality
	 */

	private function metaBox_number($post, $args) {

		//echo 'args';

		$field = $args['field'];

		$fieldId = $this->dm->prefix . '_' . $field->name;
		$fieldNonce = $fieldId . '_nonce';

		wp_nonce_field($fieldId, $fieldNonce);

		$val = '';

		if ($post != null) {
			$type = array_key_exists('type', $args) ? $args['type'] : 'post';

			$val = $this->dm->modules['core_data']->getValueForField($post->ID, $field, false, $type);

		}

		$out = '<input id="' . $fieldId . '" name="' . $fieldId . '" type="text" value="' . $val . '">';
		echo $out;

	}

	private function metaBox_text($post, $args) {

		//print_r($args);

		$field = $args['field'];
		$fieldId = $this->dm->prefix . '_' . $field->name;
		$fieldNonce = $fieldId . '_nonce';

		//echo($fieldId);

		wp_nonce_field($fieldId, $fieldNonce);

		$val = '';

		if ($post != null) {
			$type = array_key_exists('type', $args) ? $args['type'] : 'post';

			$val = $this->dm->modules['core_data']->getValueForField($post->ID, $field, false, $type);
		}

		//$val = get_post_meta( $post->ID, $fieldId, true );

		$out = '<input id="' . $fieldId . '" name="' . $fieldId . '" type="text" value="' . $val . '">';
		echo $out;

	}

	private function metaBox_option($post, $args) {

		//print_r($args);

		$field = $args['field'];
		$fieldId = $this->dm->prefix . '_' . $field->name;
		$fieldNonce = $fieldId . '_nonce';

		//echo($fieldId);

		wp_nonce_field($fieldId, $fieldNonce);

		$val = '';

		if ($post != null) {
			$type = array_key_exists('type', $args) ? $args['type'] : 'post';

			$val = $this->dm->modules['core_data']->getValueForField($post->ID, $field, false, $type);
		}

		//$val = get_post_meta( $post->ID, $fieldId, true );

		$checked = '';
		if ($val == 1) {
			$checked = 'checked="checked"';
		}

		$out = '<input type="hidden" name="' . $fieldId . '" value="0" /><input type="checkbox" value="1" id="' . $fieldId . '" name="' . $fieldId . '" ' . $checked . ' /><label for="' . $fieldId . '">Featured</label>';
		echo $out;

	}

	public function metaBox_options($post, $args) {

		$field = $args['field'];
		$fieldId = $this->dm->prefix . '_' . $field->name;
		$fieldNonce = $fieldId . '_nonce';

		//echo($fieldId);

		wp_nonce_field($fieldId, $fieldNonce);

		$terms = array();

		$val = '';

		if ($post != null) {
			$type = array_key_exists('type', $args) ? $args['type'] : 'post';

			$val = $this->dm->modules['core_data']->getValueForField($post->ID, $field, false, $type);
		}

		if ($val != '') {
			if (strpos($val, ',')) {
				$terms = explode(',', $val);
			} else {
				array_push($terms, $val);
			}
		}

		//print_r($terms);

		// val is string Array

		echo '<pre style="display:none;">' . $val . '</pre>';

		$out = '';

		$options = $field->args['options'];
		$dropdown = $field->args['dropdown'];

		if (!isset($dropdown)) {
			$dropdown = false;
		}

		if ($dropdown) {
			$out .= '<div class="widget-dropdown-option">';
			$out .= '<div class="widget-dropdown-wrap">';
			$out .= '<div class="widget-dropdown-option-title">' . $field->args['label'] . '</div>';

		}

		$out .= '<input name="' . $fieldId . '[]" value="placeholder_si_1" type="hidden">';
		$out .= '<input name="' . $fieldId . '[]" value="placeholder_si_2" type="hidden">';

		$out .= '<ul>';

		foreach ($options as $option) {

			$checked = '';
			if (in_array($option['value'], $terms)) {
				$checked = 'checked="checked"';
			}

			$out .= '<li>';
			$out .= '<label for="' . $fieldId . '_' . $option['value'] . '"><input type="checkbox" value="' . $option['value'] . '" id="' . $fieldId . '_' . $option['value'] . '" name="' . $fieldId . '[]" ' . $checked . ' />' . $option['label'] . '</label>';
			$out .= '</li>';

		}

		$out .= '</ul>';

		if ($dropdown) {
			$out .= '</div>';
			$out .= '</div>';
		}

		echo $out;

	}

	private function metaBox_extendedText($post, $args) {

		$field = $args['field'];
		$fieldId = $this->dm->prefix . '_' . $field->name;
		$fieldNonce = $fieldId . '_nonce';

		wp_nonce_field($fieldId, $fieldNonce);

		$val = '';

		if ($post != null) {
			$type = array_key_exists('type', $args) ? $args['type'] : 'post';

			$val = $this->dm->modules['core_data']->getValueForField($post->ID, $field, false, $type);
		}

		$settings = array('textarea_name' => $fieldId);
		wp_editor(htmlspecialchars_decode($val), $fieldId, $settings);

	}

	private function metaBox_choose($post, $args) {

		$field = $args['field'];
		$fieldId = $this->dm->prefix . '_' . $field->name;
		$fieldNonce = $fieldId . '_nonce';

		wp_nonce_field($fieldId, $fieldNonce);
		//$val = get_post_meta( $post->ID, $fieldId, true );

		$val = '';

		if ($post != null) {
			$type = array_key_exists('type', $args) ? $args['type'] : 'post';

			$val = $this->dm->modules['core_data']->getValueForField($post->ID, $field, false, $type);
		}

		$options = $field->args['options'];

		$dropdown = $field->args['dropdown'];
		if (!isset($dropdown)) {
			$dropdown = true;
		}

		if ($dropdown) {

			$out .= '<select id="' . $fieldId . '" name="' . $fieldId . '">';

			foreach ($options as $option) {
				$out .= '<option ' . ($option['value'] === $val ? 'selected' : '') . ' value="' . $option['value'] . '">' . $option['label'] . '</option>';
			}

			$out .= '</select>';

		} else {

			$out .= '<ul>';

			foreach ($options as $option) {
				$out .= '<li><input type="radio" value="' . $option['value'] . '" id="' . $fieldId . '_' . $option['value'] . '" name="' . $fieldId . '" ' . ($option['value'] === $val ? 'checked="checked"' : '') . ' /><label for="' . $fieldId . '_' . $option['value'] . '">' . $option['label'] . '</label></li>';
			}
			$out .= '</ul>';

		}

		echo $out;

	}

	private function metaBox_imageSelection($post, $args) {

		$field = $args['field'];
		$fieldId = $this->dm->prefix . '_' . $field->name;
		$fieldNonce = $fieldId . '_nonce';

		wp_nonce_field($fieldId, $fieldNonce);

		$val = '';

		if ($post != null) {
			$type = array_key_exists('type', $args) ? $args['type'] : 'post';

			$val = $this->dm->modules['core_data']->getValueForField($post->ID, $field, false, $type);
		}

		$image = wp_get_attachment_image_src($post->ID, 'large');

		if (!$image) {
			return;
		}

		$out .= '


    	<div class="field-wrap ' . $fieldId . '_wrap">

    	</div>


    	<input id="' . $fieldId . '" type="hidden" name="' . $fieldId . '" value="' . htmlspecialchars($val) . '" />

    	<script>

    		var $ = jQuery;

    		$(function(){

    			var image_selection_tool = new imageSelectionTool("' . $fieldId . '", "' . $image[0] . '");

    			image_selection_tool.init();

    		});

    	</script>';

		echo $out;

	}

	private function metaBox_file($post, $args) {

		$field = $args['field'];
		$fieldId = $this->dm->prefix . '_' . $field->name;
		$fieldNonce = $fieldId . '_nonce';

		wp_nonce_field($fieldId, $fieldNonce);

		$val = '';

		if ($post != null) {
			$type = array_key_exists('type', $args) ? $args['type'] : 'post';

			$val = $this->dm->modules['core_data']->getValueForField($post->ID, $field, false, $type);
		}

		$types = $field->args['types'];

		$count = -1;

		if (isset($field->args['number']) && !empty($field->args['number'])) {
			$count = $field->args['number'];
		}

		//$files = explode(',', $val);

		$fileNames = array();
		$fileIds = array();

		for ($f = 0; $f < count($val); $f++) {

			$fPath = get_attached_file($val[$f]);

			if ($fPath == '' || !$fPath) {
				continue;
			}

			array_push($fileNames, basename($fPath));
			array_push($fileIds, $val[$f]);
		}

		$out = '';

		$out .= '

    	<div class="fileLoader fileLoader_' . $fieldId . '">

    		<input id="' . $fieldId . '" class="file_value" type="hidden" name="' . $fieldId . '" value="' . implode(',', $fileIds) . '" names="' . implode(',', $fileNames) . '">

    	</div>

    	';

		if (is_admin()) {

			$out .= '

    		<script>

    			var $ = jQuery;

    			$(function(){

    				var _fileLoader = new fileLoader("fileLoader_' . $fieldId . '", JSON.parse(\' ' . json_encode($types) . ' \'), ' . $count . ');

    				_fileLoader.init();

    			});

    		</script>';

		} else {

			foreach ($files as $file) {
				if (wp_attachment_is_image($file)) {
					$out .= wp_get_attachment_image($file);
				}
			}

		}

		echo $out;

		/*$simpleFieldNonce = $fieldId . '_simple_nonce';

		wp_nonce_field($fieldId . '_simple', $simpleFieldNonce);

		$out2 = '

    	<label class="screen-reader-text" for="' . $fieldId . '_simple">File Upload</label>
    	<input id="' . $fieldId . '_simple" type="file" name="' . $fieldId . '_simple" />

    	';

		echo $out2;*/

	}

	private function metaBox_date($post, $args) {

		//print_r($args);

		$field = $args['field'];
		$fieldId = $this->dm->prefix . '_' . $field->name;
		$fieldNonce = $fieldId . '_nonce';

		//echo($fieldId);

		wp_nonce_field($fieldId, $fieldNonce);
		//$val = get_post_meta( $post->ID, $fieldId, true );

		$val = '';

		if ($post != null) {
			$type = array_key_exists('type', $args) ? $args['type'] : 'post';

			$val = $this->dm->modules['core_data']->getValueForField($post->ID, $field, false, $type);
		}

		$out = '<input id="' . $fieldId . '_temp" name="' . $fieldId . '_temp" type="text" value="' . ((isset($val) && $val != "") ? date("d M Y H:i", $val) : '') . '">';
		$out .= '<input id="' . $fieldId . '" name="' . $fieldId . '" type="hidden" value="' . $val . '">';

		$out .= '
    	<script>

    		var $ = jQuery;

    		$( function() {
    			$( "#' . $fieldId . '_temp" ).datetimepicker({
    				dateFormat: "dd M yy",
    				timeFormat: "HH:mm",
    				controlType: "select",
    				inline:true,
    				oneLine:true
    			});

    			$( "#' . $fieldId . '_temp" ).on("change", function(e){
    				var val = $( "#' . $fieldId . '_temp" ).val();
    				var out = 0;
    				if(val.length < 1 || val == null || val == undefined || val == "") {
    					out = "";
    				}
    				else {
    					out = new Date(val).getTime()/1000;

                    //out = Date.parse(val)/1000;

    				}

    				/*console.log("input");
    				console.log(out);

    				console.log("offset");
    				console.log(new Date().getTimezoneOffset());

    				console.log("pre parse date: "+val);
    				console.log("post parse date: "+new Date(out*1000).toLocaleTimeString());*/

    				$( "#' . $fieldId . '" ).val((out));
    			});
    		});

    	</script>
    	';

		echo $out;

	}

	/**
	 * Add to post form tag to allow image uploads
	 * @param wp_post $post
	 */
	public function addFormMultipartEncoding($post = null) {

		echo ' enctype="multipart/form-data"';

	}

	/**
	 * Some generated style rules, form post type icons here
	 */
	public function addDynamicAdminStyles() {

		$style = '<style>';

		for ($p = 0; $p < count($this->dm->postTypes); $p++) {

			$tmpPostType = $this->dm->postTypes[$p];

			$icon = '\f227';

			if (array_key_exists('customIcon', $tmpPostType->args)) {
				$icon = $tmpPostType->args['customIcon'];
			}

			$style .= '#adminmenu .menu-icon-' . $this->dm->prefix . '_' . $tmpPostType->name . ' div.wp-menu-image:before {';
			$style .= 'content: "' . $icon . '";';
			$style .= '}';

		}

		$style .= '</style>';

		echo $style;

	}

	/**
	 * enqueue admin scripts
	 * @param ? $hook
	 */
	public function enqueueScripts($hook) {

		//???
		wp_enqueue_media();

		wp_enqueue_script('jquery-ui');
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_script('jquery-ui-widget');
		wp_enqueue_script('jquery-ui-slider');
		wp_enqueue_style('jquery-ui-style', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');

		wp_enqueue_script('jquery-ui-datetimepicker', SI_PLUGIN_URL . 'includes/classes/modules/core/admin/includes/js/timepicker.js', array(), false, true);
		wp_enqueue_style('jquery-ui-datetimepicker-style', SI_PLUGIN_URL . 'includes/classes/modules/core/admin/includes/css/timepicker.css');

		wp_enqueue_style('dm_admin_style', SI_PLUGIN_URL . 'includes/classes/modules/core/admin/includes/css/admin_style.css');

		wp_enqueue_script('dm_fileLoader', SI_PLUGIN_URL . 'includes/classes/modules/core/admin/includes/js/fileLoader.js', false, true);
		wp_enqueue_script('dm_imageSelectionTool', SI_PLUGIN_URL . 'includes/classes/modules/core/admin/includes/js/imageSelectionTool.js', false, true);

		wp_enqueue_script('dm_admin_scripts', SI_PLUGIN_URL . 'includes/classes/modules/core/admin/includes/js/admin_scripts.css', array(), false, true);

	}

}