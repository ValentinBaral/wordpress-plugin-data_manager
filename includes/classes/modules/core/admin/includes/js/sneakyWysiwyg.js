//MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

tinymce.PluginManager.add('wpAddMedia', function(editor, url) {
  // Add a button that opens a window
	editor.addButton('add_media', {
    	text: 'Add Media',
    	icon: false,
    	onclick: function(e) {

    		console.log('button click');
    		console.log(e);

    		//console.log(wp.media.editor);
    		//console.log(wp.media.editor.send);

    		/*wp.media.editor.send.attachment = function(props, attachment) {


				//_outputName.text( attachment.filename );

				console.log(props);
				console.log(attachment);

			};

			wp.media.editor.open($(e.currentTarget));*/


			/*var mediaWindow = wp.media({
		        title: 'Add media',
		        button: {
		            text: 'Insert into post'
		        },
		        multiple: false  // Set this to true to allow multiple files to be selected
		    })
		    .on('select', function() {
		        var attachment = mediaWindow.state().get('selection').first().toJSON();
		        console.log(attachment);
		    })
		    .open();*/
		    
		    /*var clone = wp.media.gallery.shortcode;
		    wp.media.gallery.shortcode = function(attachments) {
				console.log(attachments);

				wp.media.gallery.shortcode = clone;

				// ????
				var shortcode= new Object();
			    shortcode.string = function() {return ''};
			    return shortcode;
			}*/

			//console.log(editor.$);

			editor.focus();

			wp.media.editor.open($(editor.getBody()).attr('id'));



    	}
	});
  
  // Adds a menu item to the tools menu
  /*editor.addMenuItem('example', {
    text: 'Example plugin',
    context: 'tools',
    onclick: function() {
      // Open window with a specific url
      editor.windowManager.open({
        title: 'TinyMCE site',
        url: 'http://www.tinymce.com',
        width: 800,
        height: 600,
        buttons: [{
          text: 'Close',
          onclick: 'close'
        }]
      });
    }
  });*/
});




var sneakyWysiwyg = function(_inputTargetClass){

	var self = this;

	var wrap = null;

	var editorInstance = null;

	var inputTargetClass = _inputTargetClass;

	this.init = function(){

		console.log('init@sneakyWysiwyg '+inputTargetClass);

		// build
		self.modifyDom();


	}

	this.modifyDom = function(){

		var _inputTarget = $(document).find('.'+_inputTargetClass);

		var sneakyWysiwygDom = $('<div>', {class:_inputTargetClass+'_temp'});

		sneakyWysiwygDom.html(_inputTarget.attr('value'));

		if(wrap != null){
			wrap.html('');

		}else{
			wrap = $('<div>', {class:_inputTargetClass+'_temp_wrap'});
			_inputTarget.after(wrap);
		}

		wrap.append(sneakyWysiwygDom);


		/*editorInstance = tinymce.createEditor('id',{
			selector: '.'+_inputTargetClass+'_temp',
			height: 250,
			inline:true,
			hidden_input: false,
			init_instance_callback: function (editor) {
			    editor.on('change', function (e) {
			    	console.log(e);
					$(document).find('.'+_inputTargetClass).attr('value', e.target.getContent());
			    	console.log(e.target.getContent());
			    });
			},
			plugins: [
		    	'tabfocus,paste,media,textcolor,colorpicker,link,code,wpAddMedia'
			],
			menubar:'',
			toolbar1: 'bold,italic,strikethrough,bullist,numlist,blockquote,hr,alignleft,aligncenter,alignright,link,unlink,spellchecker',
			toolbar2: 'styleselect | forecolor | add_media | code'
			//content_css: [
			//	"/wp-content/themes/sas/editor-style.css"
			//]
		});*/

		tinymce.init({
			selector: '.'+_inputTargetClass+'_temp',
			height: 250,
			convert_urls : false,
			inline:true,
			hidden_input: false,
			init_instance_callback: function (editor) {
			    editor.on('change', function (e) {
			    	console.log(e);
					$(document).find('.'+_inputTargetClass).attr('value', e.target.getContent());
			    	console.log(e.target.getContent());
			    });
			},
			plugins: [
		    	'tabfocus,paste,media,textcolor,colorpicker,link,code,wpAddMedia'
			],
			apply_source_formatting:false,
			wpautop:true,
			menubar:'',
			toolbar1: 'bold,italic,strikethrough,bullist,numlist,blockquote,hr,alignleft,aligncenter,alignright,link,unlink,spellchecker',
			toolbar2: 'styleselect | forecolor | add_media | code,removeformat'
			//content_css: [
			//	"/wp-content/themes/sas/editor-style.css"
			//]
		});

	}

	this.blur = function(){
		//wrap.find('.'+_inputTargetClass+'_temp_wrap').blur();

		console.log('blur?');

		for (var i = tinymce.editors.length - 1; i >= 0; i--) {
		    if (tinymce.editors[i].inline)
		        tinymce.editors[i].remove();
		};

		//$(document).focus();

		//editorInstance.hide();
	}
	
}