

imageSelectionTool = function(targetId, imageUrl){

	var canvas = null,
        context = null,
        rect = {},
        drag = false,
        img = document.createElement("img"),
        imageName,
        left = 0,
        top = 0, 
        right = 1,
        bottom = 1,
        canvasWidth = 600,
        canvasHeight = 600,
        imgWidth = 0, 
        imgHeight = 0;


    var valueToSet = '';
    var previousValue = '';

	var self = this;
	var _targetId = targetId;
	var _imageUrl = imageUrl;

	var selectionChange = false;
	var selectionStart = null;
	var selectionChangeSides = {x:0,y:0,w:0,h:0};

	var $canvasWrap = null;

	var $selection = null;

	this.init = function(){

		previousValue = $('#'+_targetId).val();

		if(previousValue != ''){
			previousValue = JSON.parse(previousValue);
			valueToSet = previousValue;


			valueToSet.left = parseFloat(valueToSet.left);
			valueToSet.top = parseFloat(valueToSet.top);
			valueToSet.right = parseFloat(valueToSet.right);
			valueToSet.bottom = parseFloat(valueToSet.bottom);

		}

		self.buildDom();

		setTimeout(function(){


		   	var ratio = img.naturalWidth / img.naturalHeight;
		    if (img.naturalHeight > canvasHeight && ratio < 1) {
		    	imgWidth = canvasHeight * ratio;
		    	imgHeight = canvasHeight;
			} else if (img.naturalWidth > canvasWidth && ratio > 1) {
				imgWidth = canvasWidth;
				imgHeight = canvasWidth / ratio;
			} else {
				imgWidth = img.naturalWidth;
				imgHeight = img.naturalHeight;
			}
			self.drawImg();
		   	//self.updateResult(_imageUrl, 0, 0, 1, 1);


		   	self.drawImg();

		   	if(previousValue != ''){

		   		rect.startX = valueToSet.left * imgWidth;
		   		rect.startY = valueToSet.top * imgHeight;
	            
	            rect.w = (valueToSet.right - valueToSet.left) * imgWidth;
	            rect.h = (valueToSet.bottom - valueToSet.top) * imgHeight;

	            context.fillStyle = "rgba(255, 0, 0, 0.3)";
	            context.fillRect(rect.startX, rect.startY, rect.w, rect.h);

	        }

		   	console.log(imgWidth);
		   	console.log(imgHeight);

			console.log(valueToSet);
			console.log(previousValue);

			//self.updateSelection(valueToSet);


	   },500);

		//if($imageSelectionToolModal == null) self.buildDom();

	}		   

	this.isInt = function(value) {
		return !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
	}

	this.buildDom = function(){

		var $show = $('<button>', {class:'imageSelectionTool_show', type:'button', text:'set region'});
		var $clear = $('<button>', {class:'imageSelectionTool_clear', type:'button', text:'clear'});

		$('.'+_targetId+'_wrap').append($show);
		$('.'+_targetId+'_wrap').append($clear);

		var $modal = $('<div>', {class:'imageSelectionTool_wrapper'});
		$canvasWrap = $('<div>', {class:'imageSelectionTool_inner'});

		$modal.append($canvasWrap);

		canvas = $('<canvas width="600" height="600" class="imageSelectionTool_canvas">')[0];
		context = canvas.getContext("2d");
		var $confirm = $('<button>', {class:'imageSelectionTool_confirm', text:'set', type:'button'});
		var $close = $('<button>', {class:'imageSelectionTool_close', text:'close', type:'button'});

		$canvasWrap.append($(canvas));
		$modal.append($confirm);
		$modal.append($close);

		//$canvasWrap.append($selection);

		//$('body').append($modal);

		$('#'+_targetId).after($modal)

		$show.on('click', function(e){
			$modal.addClass('active');
		});

		$clear.on('click', function(e){
			$('#'+_targetId).val('');
		});

		$confirm.on('click', function(e){
			$modal.removeClass('active');
			$('#'+_targetId).val(JSON.stringify(valueToSet));
		});

		$close.on('click', function(e){
			$modal.removeClass('active');
		});

		//$imageSelectionToolModal = $modal;

		// Image for loading  
		img.addEventListener("load", function() {
			var ratio = img.naturalWidth / img.naturalHeight;
	        if (img.naturalHeight > canvasHeight && ratio < 1) {
	            imgWidth = canvasHeight * ratio;
	            imgHeight = canvasHeight;
	        } else if (img.naturalWidth > canvasWidth && ratio > 1) {
	            imgWidth = canvasWidth;
	            imgHeight = canvasWidth / ratio;
	        } else {
	            imgWidth = img.naturalWidth;
	            imgHeight = img.naturalHeight;
	        }

	        $canvasWrap.height(imgWidth);
	        $canvasWrap.height(imgHeight);

	        self.drawImg();
	    }, false);

	    // To enable drag and drop
	    canvas.addEventListener("dragover", function(evt) {
	        evt.preventDefault();
	    }, false);

	    // Detect mousedown
	    canvas.addEventListener("mousedown", function(e) {

	    	console.log("mousedown");
	    	console.log(e);

	        rect.startX = e.layerX;
	        rect.startY = e.layerY;
	        drag = true;
	    }, false);

	    // Detect mouseup
	    canvas.addEventListener("mouseup", function(e) {
	        drag = false;
	    }, false);

	    // Draw, if mouse button is pressed
	    canvas.addEventListener("mousemove", function(e) {
	        if (drag) {
	            self.drawImg();
	            
	            rect.w = (e.layerX) - rect.startX;
	            rect.h = (e.layerY) - rect.startY;
	            left = rect.startX / imgWidth;
	            top = rect.startY / imgHeight;
	            right = (rect.w + rect.startX) / imgWidth;
	            bottom = (rect.h + rect.startY) / imgHeight;

				/*console.log("rect.w "+rect.w);
				console.log("rect.h "+rect.h);


				console.log("rect.startX "+rect.startX);
				console.log("rect.startY "+rect.startY);

				console.log("top "+top);
				console.log("bottom "+bottom);
				console.log("left "+left);
				console.log("right "+right);*/

	            if (imageName) {
	            	self.updateResult(imageName, left.toFixed(2), top.toFixed(2), right.toFixed(2), bottom.toFixed(2));
	      }
	            context.fillStyle = "rgba(255, 0, 0, 0.3)";
	            context.fillRect(rect.startX, rect.startY, rect.w, rect.h);
	        }
	    }, false);

		img.src = _imageUrl;
		imageName = _imageUrl;
	   	//self.updateResult(_imageUrl, 0, 0, 1, 1);

	}

	this.clearCanvas = function() {
        // context.clearRect(0, 0, canvasWidth, canvasHeight);
        context.fillStyle = "#ffffff";
    	context.fillRect(0, 0, canvasWidth, canvasHeight);
   	}

	this.drawImg = function() {

		if(imgWidth == 0 || imgHeight == 0) return;

        self.clearCanvas();
        context.drawImage(img, 0, 0, imgWidth, imgHeight);
    }

    this.updateResult = function(name, left, top, right, bottom) {
	    var dataFocus = '<img src="' 
	      + name + '" alt=""' + ' data-focus-left="' 
	      + left + '" data-focus-top="' 
	      + top + '" data-focus-right="' 
	      + right + '" data-focus-bottom="' 
	      + bottom + '"/>';

		var out = {}
		//out.name = name;
		out.left = left;
		out.top = top;
		out.right = right;
		out.bottom = bottom;

		valueToSet = out;

		//$('#'+_targetId).val(JSON.stringify(out));

    }

	this.show = function(id){
		//if($imageSelectionToolModal.hasClass('open')) return;

		//$imageSelectionToolModal.addClass('open');



	}

	this.close = function(){
		//$imageSelectionToolModal.removeClass('open');

		
		
	}

}