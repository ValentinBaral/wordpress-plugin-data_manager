fileLoader = function(targetClass, types, limitFiles){
	var self = this;

	var _targetClass = targetClass;
	var _types = types;
	var _limitFiles = limitFiles;

	var _addButton = null;
	var _removeButton = null;
	var _field = null;
	var _outputStatus = null;
	var _outputName = null;

	var _attachment_ids = [];
	var _attachment_names = [];
	var _attachment_list = null;

	this.init = function(){

		if(_limitFiles == undefined || !self.isInt(_limitFiles)) _limitFiles = 1;

		var _target = $(document).find('.'+_targetClass);
		console.log('init@fileLoader');

		_outputStatus = $('<div>', {class:'file_return'});
		_attachment_list = $('<div>', {class:'file_list'});
		_addButton = $('<a>', {class:'file_set', text:'+ Add file'});

		_attachment_list.sortable({
			update: function( event, ui ) {

				_attachment_ids = [];
				_attachment_names = [];

				_attachment_list.children().each(function(){
					_attachment_names.push($(this).find('.name').text());
					_attachment_ids.push(parseInt($(this).attr('attachmentId')));
				});

				self.setValue();
			}
		});
		_attachment_list.disableSelection();

		_target.append(_outputStatus);
		_target.append(_attachment_list);
		_target.append(_addButton);

		_field = _target.find('.file_value');

		self.getValues();

		$('.'+_targetClass).on('click', '.file_set', function(e) {

			e.preventDefault();
			console.log("click");

			//console.log(_attachment_list.children().length);

			if(_limitFiles <= _attachment_list.children().length && _limitFiles != -1){
				_outputStatus.text("maximum number of files added.");
				_outputStatus.addClass("warning");
				return;
			}
			
			wp.media.editor.send.attachment = function(props, attachment) {
				_outputStatus.show();
				_outputStatus.removeClass("warning");

				console.log(attachment);

				if( false == self.checkAttachment(attachment) ){
					_outputStatus.text("wrong file type.");
					_outputStatus.addClass("warning");
			       	return false;
				}

				_attachment_ids.push(attachment.id);

				var $attachmentEntry = self.buildAttachment(attachment.id, attachment.filename);
				_attachment_list.append($attachmentEntry);

				self.setValue();

				_outputStatus.text("file added");

			};

			wp.media.editor.open($(this));

			return false;

		});

		$('.'+_targetClass + ' .file_list').on("click", ".file_remove", function(e) {

			e.preventDefault();
			var $attachment = $(this).closest(".file_loader_attachment");
			var id = parseInt( $attachment.attr("attachmentId") );
			console.log(id);
			var index = _attachment_ids.indexOf(id);
			console.log(index);
			console.log(_attachment_ids);

			if (index > -1) {	
			    _attachment_ids.splice(index, 1);
			}

			self.setValue();

			$attachment.remove();

		});

		//var attachmentNames = _field.attr('names').split(',');


		console.log(_attachment_names);

		for(var a=0; a<_attachment_ids.length; a++){
			console.log('attatchment'+_attachment_names[a]);
			$attachmentEntry = self.buildAttachment(_attachment_ids[a], _attachment_names[a]);
			_attachment_list.append($attachmentEntry);
		}

	}		   

	this.checkAttachment = function(attachment){

		if(_types == 'any') return true;

		if(_types.indexOf(attachment.type) > -1){
			return true;
		}

		return false;

    }

    this.setValue = function(){

    	if(_attachment_ids.length < 1){
    		_field.attr("value", "");
    		_field.attr("names", "");
    	}else if(_limitFiles == 1){
			_field.attr("value", _attachment_ids[0]);
			_field.attr("names", _attachment_names[0]);
    	}else{
			_field.attr("value", _attachment_ids);
			_field.attr("names", _attachment_names);
    	}

    }

    this.getValues = function(){

		var values = _field.val().split(",");
		var names = _field.attr('names').split(',');

		_attachment_ids = [];
		_attachment_names = [];

		for(var i=0; i<values.length; i++){
			if(self.isInt(values[i])){
				_attachment_ids.push(parseInt( values[i] ));
				_attachment_names.push(names[i]);
			}	
		}

		//_attachment_ids = values;
		//_attachment_names = names;

	}

	this.isInt = function(value) {
		return !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
	}

	this.buildAttachment = function(id, name){

		console.log('buildAttachment '+name);

		var $att = $("<div>", {class: "file_loader_attachment", attachmentId: id});
		var $attname = $("<p>", {class: "name", text: name});
		var $attremove = $("<button>", {class: "file_remove button", text: "x"});
		$att.append($attname);
		$att.append($attremove);
		return $att;

	}

}