<?php

class dm_Api {

	private static $instance = null;

	public static function get_instance() {
		if (null == self::$instance) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private function __construct() {}

	private $dm = null;

	public function init($dm) {
		$this->dm = $dm;

		add_action('rest_api_init', array(self::$instance, 'modifyRestApiOutput'));
	}

	/**
	 * Add fields to each post type's REST API endpoint
	 *
	 */
	public function modifyRestApiOutput() {

		foreach ($this->dm->postTypes as $postType) {

			$postTypeId = $this->dm->prefix . '_' . $postType->name;

			if (array_key_exists('native_type', $postType->args) && $postType->args['native_type'] == true){
				$postTypeId = $postType->name;
			}

			// add fields for meta fields
			foreach ($postType->fields as $field) {

				register_rest_field($postTypeId, $this->dm->prefix . '_' . $field->name, array(
					'get_callback' => function ($post, $fieldName) {

						$field = self::$instance->dm->getFieldForName($fieldName);

						if ($field == null) {
							return $fieldName;
						}

						$id = -1;
						$kind = 'post';


						$type = gettype($post);

						if ($type == 'array') {
							$id = $post['id'];

							// so on the update function I get a WP_Comment when updateding meta for a comment, here I'm just getting an array where the only identification is that there is a 'post' key
							if (array_key_exists('post', $post)) {
								$kind = 'comment';
							}

						} elseif ($type == 'object') {
							$class = get_class($post);

							// wordpress's great consistency in naming their IDs right here
							if ($class == 'WP_Comment') {
								$id = $post->comment_ID;
								$kind = 'comment';
							} elseif ($class == 'WP_Term') {
								$id = $post->term_id;
								$kind = 'term';
							} elseif ($class == 'WP_User') {
								$id = $post->ID;
								$kind = 'user';
							} else {
								$id = $post->ID;
							}

						}

						if ($id == -1) {
							return $fieldName;
						}

						return self::$instance->dm->modules['core_data']->getValueForField($id, $field, true, $kind);

					},
					'update_callback' => function ($value, $post, $fieldName) {

						$field = self::$instance->dm->getFieldForName($fieldName);

						if ($field == null) {
							return false;
						}

						$id = -1;
						$kind = 'post';


						$type = gettype($post);

						// maybe posts here passed as WP_Post here aswell...
						if ($type == 'array') {
							$id = $post['id'];

						} elseif ($type == 'object') {
							$class = get_class($post);

							// wordpress's great consistency in naming their IDs right here
							if ($class == 'WP_Comment') {
								$id = $post->comment_ID;
								$kind = 'comment';
							} elseif ($class == 'WP_Term') {
								$id = $post->term_id;
								$kind = 'term';
							} elseif ($class == 'WP_User') {
								$id = $post->ID;
								$kind = 'user';
							} else {
								// should catch posts
								$id = $post->ID;
							}

						}

						if ($id == -1) {
							return false;
						}

						self::$instance->dm->modules['core_data']->writeMetaValue($value, $id, $field, $kind);

						return true;

					},
					'schema' => array(
						'description' => __('Description.'),
						'type' => array_key_exists('output_type', $field->args) ? $field->args['output_type'] : 'string',
					),
				));

			}

			// add fields for taxonomies
			foreach ($postType->taxonomies as $taxonomy) {

				register_rest_field($postTypeId, $this->dm->prefix . '_' . $taxonomy->name, array(
					'get_callback' => function ($post, $taxonomyName) {

						$taxonomy = self::$instance->dm->getTaxonomyForName($taxonomyName);

						if ($taxonomy == null) {
							return $taxonomyName;
						}

						return self::$instance->dm->modules['core_data']->queryTaxonomyTerms($taxonomy, array(), $post['id']);

					},
					'update_callback' => null,
					'schema' => array(
						'description' => __('Description.'),
						'type' => 'array',
					),
				));

			}

			// add basic fields
			register_rest_field($postTypeId, 'featured_image', array(
				'get_callback' => function ($post, $fieldName) {

					$thumbnail = get_post_thumbnail_id($post['id']);

					if ($thumbnail == '') return '';

					$out = [
						'large' => wp_get_attachment_image_src($thumbnail,'large'),
						'small' => wp_get_attachment_image_src($thumbnail,'thumbnail')
					];

					return $out;

				},
				'update_callback' => null,
				'schema' => array(
					'description' => __('Description.'),
					'type' => 'array',
				),
			));

		}

		// hook to allow other modules add their own fields, TODO: Maybe core data should do the same?
		apply_filters($this->dm->prefix . '_api_register_fields', self::$instance);

	}

}