<?php

class dm_Taxonomy {

	public $name;
	public $args;
	public $fields;
	public $overwriteArgs;

	function __construct($name, $args, $overwriteArgs) {

		$this->name = $name;
		$this->args = $args;
		$this->fields = array();
		$this->overwriteArgs = $overwriteArgs;

	}

	public function addField($field) {
		array_push($this->fields, $field);
	}

}
