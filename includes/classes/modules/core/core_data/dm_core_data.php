<?php

/**
 * This handles the registration and storage of our data
 *
 * TODO: Add functions create post (again)
 */

class dm_CoreData {

	private static $instance = null;

	public static function get_instance() {
		if (null == self::$instance) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private $dm = null;

	public function init($dm) {
		$this->dm = $dm;

		// set priority to 0 so it is fired prior to widget init @https://codex.wordpress.org/Plugin_API/Action_Reference
		add_action('init', array(self::$instance, 'createStructure'), 0);

		add_filter($this->dm->prefix . '_query', array(self::$instance, 'queryData'), 10, 2);
		add_filter($this->dm->prefix . '_query_taxonomy', array(self::$instance, 'queryTaxonomy'), 10, 1);
		add_filter($this->dm->prefix . '_get_value', array(self::$instance, 'getValueForFieldByName'), 10, 4);

		// add more mimetypes so we can have xml
		add_filter('upload_mimes', array(self::$instance, 'modifyMimeTypes'));

	}

	/**
	 * this will start registering post types and taxonomies
	 * taxonomies first as we need them for reference when creating post types
	 */
	public function createStructure() {

		$this->registerTaxonomies();
		$this->registerPostTypes();

	}

	/**
	 * [getValueForFieldByName description]
	 * @param integer $postId – Post ID, can also be term or user ID
	 * @param dm_Field $field – Reference of field
	 * @param boolean $filter – If value should be filtered as in for output
	 * @param string $kind – If this is post, term or user
	 * @return Any | '' – Value filterd according of field type, empty string if not found TODO: return error
	 */
	public function getValueForFieldByName($postId, $fieldName, $filter = true, $kind = 'post') {

		$field = $this->dm->getFieldForName($fieldName);
		if ($field == null) {
			return '';
		}

		return $this->getValueForField($postId, $field, $filter, $kind);

	}

	/**
	 * [getValueForField description]
	 * @param integer $postId – Post ID, can also be term or user ID
	 * @param dm_Field $field – Reference of field
	 * @param boolean $filter – If value should be filtered as in for output
	 * @param string $kind – If this is post, term or user
	 * @return Any
	 */
	public function getValueForField($postId, $field, $filter = true, $kind = 'post') {

		$fieldId = $this->dm->prefix . '_' . $field->name;

		$value = get_metadata($kind, $postId, $fieldId, true);

		$sanval;

		// apply special formating according to field type
		switch ($field->args['type']) {

		case 'extendedText':
			$sanval = htmlspecialchars_decode($value);
			break;

		case 'file':
			if ($value == '') {
				$sanval = '';
			} else {
				$ids = explode(',', $value);

				if ($filter) {

					$sanval = array();

					foreach ($ids as $fileId) {
						//$sanval[] = wp_get_attachment_url($fileId);

						$sanval[] = [
							'large' => wp_get_attachment_image_src($fileId,'large'),
							'small' => wp_get_attachment_image_src($fileId,'thumbnail')
						];
					}

				} else {

					$sanval = $ids;

				}

			}
			break;

		case 'number':
		case 'text':
		default:
			$sanval = $value;
			break;
		}

		return $sanval;

	}

	/**
	 * save field value for post, this is called from wordpress hook in dm_Admin module
	 * @param integer $post_id
	 */
	public function saveCustomFieldsForPostType($post_id) {

		$postTypeName = get_post_type($post_id);
		if ($postTypeName == false) {
			return;
		}

		$postType = $this->dm->getPostTypeForName($postTypeName);
		if ($postType == null) {
			return;
		}

		for ($c = 0; $c < count($postType->fields); $c++) {

			$tmpField = $postType->fields[$c];

			$this->saveValueForField($post_id, $tmpField);

		}

	}

	/**
	 * save field value for taxonomy term, this is called from multiple wordpress hooks in dm_Admin module
	 * @param integer $term_id
	 * @param integer $tt_id
	 * @param string $taxonomyName
	 */
	public function saveCustomFieldsForTaxonomy($term_id, $tt_id, $taxonomyName) {

		$taxonomy = $this->dm->getTaxonomyForName($taxonomyName);
		if ($taxonomy == null) {
			return;
		}

		for ($c = 0; $c < count($taxonomy->fields); $c++) {

			$tmpField = $taxonomy->fields[$c];

			$this->saveValueForField($term_id, $tmpField, 'term');

		}

	}

	/**
	 * save field value for user, this is called from multiple wordpress hooks in dm_Admin module
	 * @param integer $user_id
	 */
	public function saveCustomFieldsForUser($user_id) {

		$postType = $this->dm->getPostTypeForName('user');
		if ($postType == null) {
			return;
		}

		for ($c = 0; $c < count($postType->fields); $c++) {

			$tmpField = $postType->fields[$c];

			$this->saveValueForField($user_id, $tmpField, 'user');

		}

	}

	/**
	 * save field value for user, this is called from multiple wordpress hooks in dm_Admin module
	 * @param integer $user_id
	 */
	public function saveCustomFieldsForComment($comment_ID, $commentdata = array()) {

		$postType = $this->dm->getPostTypeForName('comment');
		if ($postType == null) {
			return;
		}

		for ($c = 0; $c < count($postType->fields); $c++) {

			$tmpField = $postType->fields[$c];

			$this->saveValueForField($comment_ID, $tmpField, 'comment');

		}

	}

	/**
	 * Validate that input and enviroment are correct
	 * @param any $value
	 * @param dm_Field $field
	 * @return boolean – Pass
	 */
	public function validateFieldValue($value, $field) {

		$fieldId = $this->dm->prefix . '_' . $field->name;
		$fieldNonce = $fieldId . '_nonce';

		if (!isset($_REQUEST[$fieldNonce])) {
			return false;
		}

		if (!wp_verify_nonce($_REQUEST[$fieldNonce], $fieldId)) {
			return false;
		}

		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return false;
		}

		if (!isset($value)) {
			return false;
		}

		return true;

	}

	/**
	 * Mostly to strip form data
	 * @param any $value
	 * @param dm_Field $field
	 * @return string – Sanitized $value
	 */
	public function sanitizeFieldValue($value, $field) {

		$fieldId = $this->dm->prefix . '_' . $field->name;

		$sanval = '';

		switch ($field->args['type']) {

		case 'number':
			$sanval = floatval($value);
			break;

		case 'text':
			$sanval = sanitize_text_field($value);
			break;

		case 'extendedText':
			$sanval = htmlspecialchars($value);
			break;

		case 'option':
			if ($value == 1) {
				$sanval = 1;
			} else {
				$sanval = 0;
			}
			break;

		case 'options':

			$items = '';
			if (is_string($value)) {
				$items = $value;
			} else {
				$items = implode(',', $value);
			}

			$sanval = strval($items);

			break;

		case 'choose':
			$sanval = $value;
			break;

		default:
			$sanval = $value;
			break;

		}

		return $sanval;

	}


	/**
	 * Write meta value to wordpress
	 * @param any $value
	 * @param integer $postId
	 * @param dm_Field $field
	 * @param string $kind
	 * @return boolean – Success
	 */
	public function writeMetaValue($value, $postId, $field, $kind = 'post') {

		$fieldId = $this->dm->prefix . '_' . $field->name;

		// because some types have special update_{{$kind}}_meta functions
		$success = update_metadata($kind, $postId, $fieldId, $value);

		if (!$success) {
			$this->dm->log('error writing meta for field: ' . $field->name . ' of kind: ' . $kind, __LINE__, __FUNCTION__, __CLASS__, true);
			return false;
		}

		return true;

	}

	/**
	 * Use wordpress functions to save meta values for field, multiple kinds are supported
	 * @param integer $postId – Post ID, can also be term or user ID
	 * @param dm_Field $field – Reference of field
	 * @param string $kind – If this is post, term or user
	 */
	public function saveValueForField($postId, $field, $kind = 'post') {

		$fieldId = $this->dm->prefix . '_' . $field->name;

		$value = $_REQUEST[$fieldId];

		$isValid = $this->validateFieldValue($value, $field);

		if (!$isValid) return;

		$sanval = $this->sanitizeFieldValue($value, $field);

		$this->writeMetaValue($sanval, $postId, $field, $kind);

	}

	/**
	 * Query posts for specified post type name
	 * @param string $postTypeName
	 * @param array $args
	 * @return wp_query | false
	 */
	public function queryData($postTypeName, $args = array()) {

		$postType = $this->dm->getPostTypeForName($postTypeName);

		if ($postType == null) {
			return false;
		}

		$postTypeId = $this->dm->prefix . '_' . $postType->name;

		if (isset($postType->args['native_type'])) {
			if ($postType->args['native_type'] == true) {
				$postTypeId = $postType->name;
			}
		}

		$defaultArgs = array(
			'posts_per_page' => 50,
			'post_type' => $postTypeId,
			'orderby' => 'post_title',
			'order' => 'ASC',
		);

		$combinedArgs = array_merge($defaultArgs, $args);

		$newQuery = new WP_Query($combinedArgs);

		return $newQuery;

	}

	/**
	 * [queryTaxonomyTermsByName description]
	 * @param string $taxonomyName – Reference to taxonomy object
	 * @param array $args (optional)
	 * @param integer $postId (optional)
	 * @return array [wp_term] | false
	 */
	public function queryTaxonomyTermsByName($taxonomyName, $args = array(), $postId = -1) {

		if (!isset($queryArgs)) {
			$queryArgs = array();
		}

		$taxonomy = $this->dm->getTaxonomyForName($taxonomyName);

		if ($taxonomy == null) {
			return false;
		}

		return $this->queryTaxonomyTerms($taxonomy, $args, $postId);

	}

	/**
	 * [queryTaxonomyTermsByName description]
	 * @param dm_Taxonomy $taxonomy – Taxonomy object
	 * @param array $args (optional)
	 * @param integer $postId (optional)
	 * @return array [wp_term] | false
	 */
	public function queryTaxonomyTerms($taxonomy, $args = array(), $postId = -1) {

		$taxonomyId = $this->dm->prefix . '_' . $taxonomy->name;

		$defaultArgs = array(
			'fields' => 'all',
		);

		$combinedArgs = array_replace($defaultArgs, $args);

		if (array_key_exists('native_taxonomy', $taxonomy->args)) {
			if ($taxonomy->args['native_taxonomy'] == true) {
				$taxonomyId = $taxonomy->name;
			}

		}

		$terms = array();

		if ($postId > -1) {
			$terms = wp_get_post_terms($postId, $taxonomyId, $combinedArgs);
		} else {
			$terms = get_terms($taxonomyId, $combinedArgs);
		}

		foreach ($terms as $term) {
			foreach ($taxonomy->fields as $field) {
				$term->{$field->name} = $this->getValueForField($term->term_id, $field, true, 'term');
			}
		}

		return $terms;

	}

	/**
	 * register our taxonomies as custom taxonomies to wordpress
	 */
	public function registerTaxonomies() {

		// register taxonomies
		for ($t = 0; $t < count($this->dm->taxonomies); $t++) {

			$tmpTaxonomy = $this->dm->taxonomies[$t];

			if (array_key_exists('native_taxonomy', $tmpTaxonomy->args)) {
				// don't register tax if it's just reference to native post type
				if ($tmpTaxonomy->args['native_taxonomy'] == true) {
					continue;
				}

			}

			$defaultTaxonomyArgs = array(
				'hierarchical' => true,
				'label' => 'Type', //Display name
				'query_var' => true,
			);

			$combinedTaxonomyArgs = array_replace($defaultTaxonomyArgs, $tmpTaxonomy->overwriteArgs);

			$slug = $this->dm->prefix . '_' . $tmpTaxonomy->name;

			register_taxonomy(
				$slug, // slug
				null, // post type name
				$combinedTaxonomyArgs // args
			);

			if (is_admin()) {

				if (array_key_exists('admin', $this->dm->modules)) {

					// add form filters that only exist indivudally for some reason
					add_action($slug . '_add_form_fields', array(self::$instance->dm->modules['admin'], 'buildNewTermForm'), 10, 1);
					add_action($slug . '_edit_form_fields', array(self::$instance->dm->modules['admin'], 'buildTermForm'), 10, 2);
					add_action($slug . '_term_edit_form_tag', array(self::$instance->dm->modules['admin'], 'addFormMultipartEncoding'), 10, 1);

				}

			}

		}

	}

	/**
	 * register our post types as custom post types to wordpress
	 * if a post type contains any taxonomy register a reference
	 */
	public function registerPostTypes() {

		// register custom post types
		for ($p = 0; $p < count($this->dm->postTypes); $p++) {

			$tmpPostType = $this->dm->postTypes[$p];

			if (array_key_exists('native_type', $tmpPostType->args)) {
				// don't register post type if it's just reference to native post type
				if ($tmpPostType->args['native_type'] == true) {
					continue;
				}

			}

			$defaultArgs = array(
				'public' => true,
				'has_archive' => false,
				'label' => $tmpPostType->name,
				'capability_type' => 'page',
				'supports' => array('title', 'editor', 'author', 'thumbnail', 'tags', 'page-attributes'),
				'menu_icon' => '',
				'show_in_rest' => true,
				'rewrite' => array('slug' => $tmpPostType->name),
			);

			$combinedArgs = array_replace($defaultArgs, $tmpPostType->overwriteArgs);

			register_post_type($this->dm->prefix . '_' . $tmpPostType->name, $combinedArgs);

			// associate post type with taxonomies, currently skips posts & pages
			for ($t = 0; $t < count($tmpPostType->taxonomies); $t++) {

				$tmpTaxonomy = $tmpPostType->taxonomies[$t];

				$taxName = $this->dm->prefix . '_' . $tmpTaxonomy->name;

				if (array_key_exists('native_taxonomy', $tmpTaxonomy->args)) {
					if ($tmpTaxonomy->args['native_taxonomy'] == true) {
						$taxName = $tmpTaxonomy->name;
					}

				}

				register_taxonomy_for_object_type($taxName, $this->dm->prefix . '_' . $tmpPostType->name);

			}

		}

	}

	/**
	 * upload an attachment and make connection to post
	 * @param blob | null $file – File we're getting from a http request
	 * @param dm_Field $field – Reference of field
	 * @param integer $postId – Post ID to link file to
	 * @return integer $attachment_id
	 *
	 * TODO: $field & $postId should not be required at this point
	 * TODO: Cleanup
	 */
	public function uploadAttatchment($file, $field, $postId) {

		if (!function_exists('media_handle_upload')) {
			require_once ABSPATH . "wp-admin" . '/includes/image.php';
			require_once ABSPATH . "wp-admin" . '/includes/file.php';
			require_once ABSPATH . "wp-admin" . '/includes/media.php';
		}

		$arr_file_type = wp_check_filetype(basename($file['name']));
		$uploaded_file_type = $arr_file_type['type'];

		// Set an array containing a list of acceptable formats
		$allowed_file_types = $field->args['types'];

		if ($allowed_file_types != 'any') {

			$correctType = false;

			foreach ($allowed_file_types as $type) {
				if (strripos($uploaded_file_type, $type) > -1) {
					$correctType = true;
				}

			}

			if (!$correctType) {
				return false;
			}

		}

		$upload_overrides = array('test_form' => false);

		$uploaded_file = wp_handle_upload($file, $upload_overrides);

		if (!isset($uploaded_file['file'])) {
			return false;
		}

		$file_name_and_location = $uploaded_file['file'];

		$file_title_for_media_library = basename($file['name']);

		// Set up options array to add this file as an attachment
		$attachment = array(
			'post_mime_type' => $uploaded_file_type,
			'post_title' => addslashes($file_title_for_media_library),
			'post_content' => '',
			'post_status' => 'inherit',
			'post_author' => 1,
		);

		// Run the wp_insert_attachment function. This adds the file to the media library and generates the thumbnails. If you wanted to attch this image to a post, you could pass the post id as a third param and it'd magically happen.
		$attach_id = wp_insert_attachment($attachment, $file_name_and_location, $postId);

		if (stripos($uploaded_file_type, 'image') > -1) {
			//require_once(ABSPATH . "wp-admin" . '/includes/image.php');
			$attach_data = wp_generate_attachment_metadata($attach_id, $file_name_and_location);
			wp_update_attachment_metadata($attach_id, $attach_data);
		}

		//if(is_wp_error($attach_id)) return false;

		return $attach_id;

	}

	/**
	 * allow xml/kml file types to be uploaded to media library
	 * @param array $existing_mimes
	 * @return array $existing_mimes
	 */
	public function modifyMimeTypes($existing_mimes) {
		$existing_mimes['xml'] = 'text/xml';
		$existing_mimes['kml'] = 'text/kml';
		$existing_mimes['png'] = 'image/png';
		//$existing_mimes['cs'] = 'text/plain';
		return $existing_mimes;
	}

}