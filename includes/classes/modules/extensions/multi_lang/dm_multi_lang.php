<?php

/**
 * TODO: Really this whole class
 */

class dm_MultiLang {

	private static $instance = null;

	public static function get_instance() {
		if (null == self::$instance) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private function __construct() {}

	private $dm = null;

	public function init($dm) {

		$this->dm = $dm;

	}

	public function getLanguages() {

		if (function_exists(pll_languages_list)) {
			return pll_languages_list(array('hide-empty' => 1, 'fields' => 'slug'));
		}

	}

	public function getLanguagesObjects() {

		return $languagesObject = pll_the_languages(array('raw' => 1));

	}

	public function getDefaultLanguage() {
		return pll_default_language('slug');
	}

	public function getPostLanguage($id) {
		return pll_get_post_language($id, 'slug');
	}

}