<?php

class dm_Connection {

	public $from;
	public $to;
	public $args;
	public $name;
	public $fields = array();

	function __construct($from, $to, $args) {

		$this->from = $from;
		$this->to = $to;
		$this->args = $args;

		$toName = '';

		if (is_string($to)) {
			$toName = $to;
		} else {
			$toName = $to->name;
		}

		$this->name = $from->name . '_to_' . $toName;

	}

	function addField($field) {
		$this->fields[] = $field;
	}

}
