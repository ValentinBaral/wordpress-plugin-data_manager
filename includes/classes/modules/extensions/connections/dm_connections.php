<?php

require_once __DIR__ . '/includes/classes/dm_connection.php';

class dm_Connections {

	private static $instance = null;

	public static function get_instance() {
		if (null == self::$instance) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private function __construct() {}

	/**
	 * Reference to all connections we have created
	 * @var array [dm_Connection]
	 */
	private $connections = [];

	private $dm = null;

	public function init($dm) {

		$this->dm = $dm;

		add_action('p2p_init', array(self::$instance, 'registerConnections'));

		add_filter($this->dm->prefix . '_con_query', array(self::$instance, 'queryPostByConnectionNameForPostId'), 10, 2);
		add_filter($this->dm->prefix . '_con_connect_items', array(self::$instance, 'connectItemsWithConnectionName'), 10, 3);

		add_filter($this->dm->prefix . '_api_register_fields', array(self::$instance, 'addToApi'), 10, 1);

	}

	/**
	 * Create connection object and store reference
	 * @param dm_PostType $from
	 * @param dm_PostType $to
	 * @param array $args
	 * @return dm_Connection
	 */
	public function createConnection($from, $to, $args = []) {

		$connection = new dm_Connection($from, $to, $args);

		$this->connections[] = $connection;

	}

	/**
	 * Get all connections includeing specified name
	 * @param String $postTypeName
	 * @return array [dm_Connection]
	 */
	public function getConnectionsForPostType($postTypeName) {

		$foundConnections = [];

		for ($c = 0; $c < count($this->connections); $c++) {
			$connection = $this->connections[$c];

			if ($connection->from->name === $postTypeName || $connection->to->name === $postTypeName) {
				$foundConnections[] = $connection;
			}

		}

		return $foundConnections;

	}

	/**
	 * Get connection by name refference
	 * @param string $connectionName
	 * @return dm_Connection
	 */
	public function getConnectionForName($connectionName) {

		$name = $connectionName;

		$prefixString = $this->dm->prefix . '_';
		$prefixPos = strpos($connectionName, $prefixString);

		if ($prefixPos > -1) {
			$name = substr($connectionName, $prefixPos . strlen($prefixString));
		}

		for ($c = 0; $c < count($this->connections); $c++) {
			$connection = $this->connections[$c];

			if ($connection->name === $name) {
				return $connection;
			}

		}

		return null;

	}

	/**
	 * Register connection types, called from p2p hook
	 */
	public function registerConnections() {

		for ($c = 0; $c < count($this->connections); $c++) {

			$tempConnection = $this->connections[$c];

			$defaultArgs = array(
				'cardinality' => 'many-to-many',
				'title' => array('from' => 'From', 'to' => 'To'),
				'sortable' => 'any',
				'fields' => array(),
			);

			$combinedArgs = array_replace($defaultArgs, $tempConnection->args);

			$connectionFields = $tempConnection->fields;

			if (count($connectionFields) > 0) {
				foreach ($connectionFields as $field) {
					$combinedArgs['fields'][$field->name] = $field->args;
				}
			}

			// make sure we don't overwrite crucial vars
			$combinedArgs['name'] = $this->dm->prefix . '_' . $tempConnection->name;

			$nativeType = false;

			if (array_key_exists('native_type', $tempConnection->from->args)) {
				if ($tempConnection->from->args['native_type'] == true) {
					$nativeType = true;
				}
			}

			if ($nativeType) {
				$combinedArgs['from'] = $tempConnection->from->name;
			} else {
				$combinedArgs['from'] = $this->dm->prefix . '_' . $tempConnection->from->name;
			}

			if (is_string($tempConnection->to)) {
				$combinedArgs['to'] = $tempConnection->to;
			} else {

				$nativeType = false;

				if (array_key_exists('native_type', $tempConnection->to->args)) {
					if ($tempConnection->to->args['native_type'] == true) {
						$nativeType = true;
					}
				}

				if ($nativeType) {
					$combinedArgs['to'] = $tempConnection->to->name;
				} else {
					$combinedArgs['to'] = $this->dm->prefix . '_' . $tempConnection->to->name;
				}
			}

			p2p_register_connection_type($combinedArgs);

		}

	}

	/**
	 * Connect two items with defined connection name
	 * @param string $connectionName
	 * @param dm_PostType $from
	 * @param dm_PostType $to
	 * @return boolean – Success
	 */
	public function connectItemsWithConnectionName($connectionName, $from, $to) {

		$connection = $this->getConnectionForName($connectionName);

		if ($connection == null) {
			return false;
		}

		return $this->connectItems($connection, $from, $to);

	}

	/**
	 * Connect two items with defined connection
	 * @param dm_Connection $connection
	 * @param dm_PostType $from
	 * @param dm_PostType $to
	 * @return boolean – Success
	 */
	public function connectItems($connection, $from, $to) {

		$connectionId = $p2p_type($connection->name)->connect($from, $to, array(
			'date' => current_time('mysql'),
		));

		if (!is_wp_error($connectionId)) {
			return true;
		}

		return false;

	}

	/**
	 * [queryPostByConnectionNameForPostId description]
	 * @param integer $postId
	 * @param string $connectionName
	 * @return [wp_posts] posts
	 */
	public function queryPostByConnectionNameForPostId($postId, $connectionName) {

		$connection = $this->getConnectionForName($connectionName);

		if ($connection == null) {
			return array();
		}

		return $this->queryPostByConnectionForPostId($postId, $connection);
	}

	/**
	 * [queryPostByConnectionForPostId description]
	 * @param integer $postId
	 * @param dm_Connection $connection
	 * @return [wp_posts] posts
	 */
	public function queryPostByConnectionForPostId($postId, $connection) {

		$connected = [];

		//$to = $connection->to;

		$fromId = get_post_type($postId);

		$from = self::$instance->dm->getPostTypeForName($fromId);

		$toName;

		if ($from->name === $connection->from->name) {

			$toName = $this->dm->prefix . '_' . $connection->to->name;

			if (array_key_exists('native_type', $connection->to->args) && $connection->to->args['native_type'] == true){
				$toName = $connection->to->name;
			}

		} else {

			$toName = $this->dm->prefix . '_' . $connection->from->name;

			if (array_key_exists('native_type', $connection->from->args) && $connection->from->args['native_type'] == true){
				$toName = $connection->from->name;
			}

		}

		$connectionQuery = new WP_Query(array(
			'posts_per_page' => -1,
			'post_type' => $toName,
			'connected_type' => $this->dm->prefix . '_' . $connection->name,
			'connected_items' => array($postId),
			'connected_order' => 'asc',
		));

		while($connectionQuery->have_posts()) {

			$connectionQuery->the_post();

			$item = new stdClass();

			$item->ID = get_the_ID();

			foreach ($connection->fields as $field) {
				$item->{$field->name} = p2p_get_meta($posts[$i]->p2p_id, $field->name, true);
			}

			$connected[] = $item;

		}

		wp_reset_postdata();

		return $connected;

	}

	public function addToApi($api) {

		foreach ($this->dm->postTypes as $postType) {

			$postTypeId = $this->dm->prefix . '_' . $postType->name;

			if (array_key_exists('native_type', $postType->args) && $postType->args['native_type'] == true){
				$postTypeId = $postType->name;
			}

			$postTypeConnections = $this->getConnectionsForPostType($postType->name);

			foreach ($postTypeConnections as $connection) {

				$connectionOutName = $connection->name;

				// needs to register for both post types $from/to

				register_rest_field($postTypeId, $this->dm->prefix . '_' . $connection->name, array(
					'get_callback' => function ($post, $connectionName) {

						$connection = self::$instance->getConnectionForName($connectionName);

						if ($connection == null) {
							return $connectionName;
						}

						return self::$instance->queryPostByConnectionForPostId($post['id'], $connection);

					},
					'update_callback' => null,
					'schema' => array(
						'description' => __('Description.'),
						'type' => 'array',
					),

				));

			}

		}

	}

}