<?php


/**
 * Define a custom prefix here
 * @param String $prefix
 * @return String
 */
function dm_custom_prefix($prefix) {
	return $prefix;
}

function dm_user_define_structure($dm) {

	/*
			Add custom post-types and field here

			EXAMPLE:

			post_type
			$points = $dm->createPostType('points', array( 'customIcon'  => '\f313' ), array( 'label'  => 'Points of interest', 'rewrite' => array('slug' => 'points' ) ));

			field
			$testField = $dm->createField( 'test', array( 'label' => 'Test', 'type' => 'text', 'output_type' => 'string', 'position' => 'side' ));

			taxonomy
			$trails = $dm->createTaxonomy('trails', array(), array('label' => 'Trails'));

			combine
			$points->addField($testField);
			$points->addTaxonomy($trails);

			connection
			$dm->modules['connections']->createConnection( $points, $points, array('cardinality' => 'many-to-many', 'title' => array( 'from' => 'From', 'to' => 'To' )) );

			reference native types:

			$pages = $dm->getPostTypeForName('page');
			$posts = $dm->getPostTypeForName('post');
			$attachments = $dm->getPostTypeForName('attachment');


		___________________________________________________________________________________________________________
			USER-DATA #START
	*/

	// your definitions here

	/*
		___________________________________________________________________________________________________________
			USER-DATA #END
	*/

}